-- ZombieMud Scripts
-- This script contains area id and pathing table and provides functions for travel alias
-- @author Antti Heikkilä

-- Delay between speedwalk commands in milliseconds
speedwalkDelay = 50

--- Default location
defaultLocation = "cs"

--- List for location information and pathing
locationList = {
  {
    id = "cs",
    description = "Central Square",
    path = ""
  },
  {
    id = "bird",
    description = "Bird Island",
    path = "12n 2(ne/sw) (village/leave) 6w 2s 3w (n/out)" -- Path from the default location
  },
  {
    id = "brownie",
    description = "Brownie fields",
    path = "19e 2n (field/leave)" -- Path from the default location
  },
  {
    id = "dusk",
    description = "Dusk\'s Realm",
    path = "9e 5n (path/out)" -- Path from the default location
  },
  {
    id = "dwarf",
    description = "Dwarf Valley",
    path = "9s 7e (valley/hills)" -- Path from the default location
  },
  {
    id = "goblin",
    description = "Goblin forest",
    path = "9s 3e (path/path)" -- Path from the default location
  },
  {
    id = "green",
    description = "Village Green",
    path = "14n 2e (village/leave)" -- Path from the default location
  },
  {
    id = "moria",
    description = "Moria & Orc Forest",
    path = "9w 20(sw/ne) 20w s 4w (trail/out)" -- Path from the default location
  },
  {
    id = "recruit",
    description = "Recruits",
    path = "13s 2(sw/ne) (in/out)" -- Path from the default location
  },
  {
    id = "purple",
    description = "Purple Sauruses",
    path = "11n 8(ne/sw) (path/path)" -- Path from the default location
  },
  {
    id = "ranger",
    description = "Ranger Guild",
    path = "9s 8w 3s 4(sw/ne) 4(se/nw) 4s 2w (forest/se) w s" -- Path from the default location
  }
}

-- Resets the current location, prints location list or runs the character to the given location. 
-- @param alias Label of the alias.
-- @param line Input line used to call the alias.
-- @param args Wildcards given to the alias.
function travel (alias, line, args)
  local currentLocationId = GetVariable("CurrentLocationId")
  if locationList == nil then -- Location list is empty
    printError("Error: Invalid location list!\nCheck the location list from the script file.")
  elseif currentLocationId == nil then -- Current location is not set
    printError("Error: Invalid location!\nReset your location using \'"..alias.." reset\'.")
  else
    if args[1] == nil or args[1] == "help" then -- Print usage
      print("Usage: "..alias.." [help] [list] [reset] [location]")
    elseif args[1] == "list" then -- Print location list
      print("You can travel to the following locations:")
      print("AREA NAME (ID)")
      for i = 1, table.getn(locationList) do
        print(locationList[i].description.." ("..locationList[i].id..")")
      end -- for
      print("Travel to location using \'"..alias.." id\'.")
    elseif args[1] == "reset" then -- Set default location as the current location
      local location = getLocation(defaultLocation)
      if location == nil then
        printError("Error: Invalid default location!\nSet the default location in the script file.")
      else
        SetVariable("CurrentLocationId", location.id)
        SetVariable("CurrentLocation", location.description)
        print("Your location was set to "..location.description..".")
      end -- if
    else -- Run character to the given location
      local location = getLocation(currentLocationId)
      local destination = getLocation(args[1])
      if location == nil then
        printError("Error: Missing location!\nAdd the current location the script file or reset it using \'"..alias.." reset\' option.")
      elseif destination == nil then
        printError("Error: Missing destination!\nAdd the location the script file.")
      elseif location == destination then
        print("You are already there.")
      else
        local path = buildPath(location, destination)
        if path == nil then
          printError("Error: Invalid path!\nCould not create path from your current location to the destination.")
        elseif string.sub(EvaluateSpeedwalk(path.path), 1, 2) == "*" then
          printError("Error: Invalid speedwalk path!\nFix the speedwalk path in the script file.")
        else
          print("Traveling to "..destination.description..".")
          SetSpeedWalkDelay(speedwalkDelay)
          DoAfterSpeedWalk(1, path.path)
          SetVariable("CurrentLocationId", destination.id)
          SetVariable("CurrentLocation", destination.description)
        end -- if
      end -- if
    end -- if
  end -- if
end -- function

-- Builds a path from the current location to the target location.
-- @return Location information array on success, nil on failure.
function buildPath (currentLocation, targetLocation)
  local result = nil
  if currentLocation.id == defaultLocation then
    return {
      from = currentLocation.id,
      to = targetLocation.id,
      path = targetLocation.path
    }
  elseif targetLocation.id == defaultLocation then
    return {
      from = currentLocation.id,
      to = targetLocation.id,
      path = ReverseSpeedwalk(currentLocation.path)
    }
  else
    return {
      from = currentLocation.id,
      to = targetLocation.id,
      path = ReverseSpeedwalk(currentLocation.path).." "..targetLocation.path
    }
  end -- if
end -- function

-- Gets location with the given id from the location list.
-- @return Location information array on success, nil on failure.
function getLocation (id)
  local result = nil
  if string.len(id) > 0 then
    for i = 1, table.getn(locationList) do
      if locationList[i].id == id then
        result = locationList[i]
        break
      end -- if
    end -- for
  end -- if
  return result
end -- function

-- Prints an error message.
-- @param message Error message.
-- @param message Debug information to the user.
function printError (message)
  print(message)
end -- function
