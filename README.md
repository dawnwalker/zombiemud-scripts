# ZombieMud Script and Plugins for MushClient #

This repository provides [MushClient](http://www.gammon.com.au/mushclient/mushclient.htm) scripts and plugins for [ZombieMud](http://zombiemud.org), which is a text-based multi-user online roleplaying game.

This repository is not official or related to MushClient or ZombieMud in any way and does not provide support for them.